using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PhysicalObject : MonoBehaviour
{
	[SerializeField]
	private CollisionManager _collisionManager;

	public MyColliderRealisation _myColliderRealisation;

	Vector3 _oldPosition;

	public float _mass = 1;
	public float _drag = 0.05f;
	public float _minSpeed = 0.1f;

	//start: not used
	[NonSerialized]
	public float _rotationalDrag = 0.05f;
	[NonSerialized]
	public float _minRotationalSpeed = 0.1f;
	//end: not used

	public Vector3 _currentVelocity = new Vector3();

	[NonSerialized]
	public float _velocityScaling = 1;

	[NonSerialized]
	public Quaternion _currentRotationFromStart;

	//just for testing
	public float _incomingMass = 2;
	public Vector3 _incomingVelocity;

	//just for testing
	[NonSerialized]
	public Vector3 _normalOfIncomingRotation;
	[NonSerialized]
	public float _incomingRotationSpeed;

	//helper for update
	int i = -1;

	//not used
	public class rotationInfo
	{
		public Vector3 _normal;

		public float _speed;

		public rotationInfo(Vector3 normal, float speed)
		{
			_normal = normal;
			_speed = speed;
		}
	}

	//not used
	public List<rotationInfo> _activeRotations = new List<rotationInfo>();

	//old rotationalVelocity with quaternions
	public class rotationalVelocity
	{
		public Quaternion rotation;

		public float speed;

		public rotationalVelocity(Quaternion quat, float f)
		{
			rotation = quat;

			speed = f;
		}
	}

	//old list
	public List<rotationalVelocity> _activeRotationsByQuats = new List<rotationalVelocity>();

	public List<PhysicalObject[]> _collisionsThisUpdate = new List<PhysicalObject[]>();

	void Awake()
	{
		_oldPosition = transform.position;	
	}
		
	void Update ()
	{
		ApplyVelocityAndDrag();

		//ApplyRotations();

		_collisionManager.GlobalCollisionCheck(this);

		//i++;

		//if (i > 2)
		//{
			
		//	i = -1;
		//}
		

	}

	private void LateUpdate()
	{
		_collisionsThisUpdate.Clear();
	}



	public void AddAxisRotation(Vector3 normal, float speed)
	{
		if (normal.magnitude == 1 && speed > 0)
		{
			_activeRotations.Add(new rotationInfo(normal, speed));
		}
		else
		{
			throw new Exception("normal not a unit or speed not positive.");
		}	
	}

	public void ApplyRotations()
	{
		for (int i = _activeRotations.Count - 1; i > -1; i--)
		{
			float[,] rotMatrix = MyMath.GetRotationMatrixFromNormalAndAngle(_activeRotations[i]._normal, _activeRotations[i]._speed * Time.deltaTime);

			ApplyRotationToTransform(rotMatrix);
			ApplyRotationToMyCollider(_myColliderRealisation, rotMatrix);

			//apply rotational drag
			_activeRotations[i]._speed = _activeRotations[i]._speed * (1 - _rotationalDrag * Time.deltaTime);

			//remove rotation if below minimum speed
			if (_activeRotations[i]._speed < _minRotationalSpeed)
			{
				_activeRotations.Remove(_activeRotations[i]);
			}
		}
	}

	private void ApplyRotationToMyCollider(MyColliderRealisation _myColliderRealisation, float[,] rotMatrix)
	{
		if (_myColliderRealisation._nameOfVisualisation == KnownVisualisations._name_myOwnCube)
		{
			Vector3[] edges = _myColliderRealisation._cubeVertices;

			for (int i = 0; i < edges.Length; i++)
			{
				edges[i] -= _myColliderRealisation._cubeCenter;
				edges[i] = MyMath.LinearMapping_Mx(rotMatrix, edges[i]);
				edges[i] += _myColliderRealisation._cubeCenter;
			}
		}
	}

	private void ApplyRotationToTransform(float[,] rotMatrix)
	{
		transform.forward = MyMath.LinearMapping_Mx(rotMatrix, transform.forward);
	}

	public void AddQuaternionRotation(Quaternion rotation, float speed)
	{
		//here could be a case, where it is checked whether the rotation is not zero.
		if (rotation != Quaternion.identity && speed > 0)
		{
			_activeRotationsByQuats.Add(new rotationalVelocity(rotation, speed));
		}
	}

	public void ApplyQuaternionRotations()
	{
		float slowDown = 1 / 10;

		for (int i = _activeRotationsByQuats.Count - 1; i >= 0 ; i--)
		{
			Vector3 v3 = _activeRotationsByQuats[i].rotation.eulerAngles;
			float f = _activeRotationsByQuats[i].speed * Time.deltaTime;
			Quaternion quat = Quaternion.Euler(v3 * f);
			
			//quat.x = _activeRotations[i].rotation.x * _activeRotations[i].speed * Time.deltaTime;
			//quat.y = _activeRotations[i].rotation.y * _activeRotations[i].speed * Time.deltaTime;
			//quat.z = _activeRotations[i].rotation.z * _activeRotations[i].speed * Time.deltaTime;
			//quat.w = _activeRotations[i].rotation.w * _activeRotations[i].speed * Time.deltaTime;

			Quaternion whut = new Quaternion(1, 1, 1, 1);

			Rotate(quat);
			_myColliderRealisation.UpdateColliderInfoForRotation(quat);
			_currentRotationFromStart = transform.rotation;

			_activeRotationsByQuats[i].speed = _activeRotationsByQuats[i].speed * (1 - _drag * Time.deltaTime * 10);

			if (_activeRotationsByQuats[i].speed < _minRotationalSpeed)
			{
				_activeRotationsByQuats.Remove(_activeRotationsByQuats[i]);
			}

		}
	}

	public void ApplyIncomingCollision(PhysicalObject otherPO)
	{
		if ( _mass > 0 && otherPO._mass > 0)
		{
			PhysicalObject[] version1 = new PhysicalObject[2] { this, otherPO };
			PhysicalObject[] version2 = new PhysicalObject[2] { otherPO, this };

			if (_collisionsThisUpdate.Contains(version1) || _collisionsThisUpdate.Contains(version2))
			{
				return;
			}

			//bc stands for before collision

			Vector3 otherVelocity_bc = otherPO._currentVelocity;
			float otherMass_bc = otherPO._mass;

			Vector3 thisVelocity_bc = this._currentVelocity;
			float thisMass_bc = this._mass;

			//thisPO collision on otherPO

			Vector3 impactDirection = (otherPO.transform.position - this.transform.position).normalized;

			Vector3 velocityOfThisInImpactDirection = Vector3.Dot(impactDirection, thisVelocity_bc) * impactDirection;

			if (Vector3.Dot(impactDirection, thisVelocity_bc) < 0)
			{
				velocityOfThisInImpactDirection = new Vector3();
			}
			
			//otherPO collision on thisPO

			Vector3 impactDirection2 = (-otherPO.transform.position + this.transform.position).normalized;

			Vector3 velocityOfOtherInImpactDirection = Vector3.Dot(impactDirection, otherVelocity_bc) * impactDirection;

			if (Vector3.Dot(impactDirection, otherVelocity_bc) < 0)
			{
				velocityOfOtherInImpactDirection = new Vector3();
			}

			//velocities get modified

			Vector3 newVelocityOfOtherPO = velocityOfThisInImpactDirection * thisMass_bc / otherMass_bc + otherVelocity_bc - velocityOfOtherInImpactDirection;

			otherPO._currentVelocity = newVelocityOfOtherPO;


			Vector3 newVelocityOfThisPO = velocityOfOtherInImpactDirection * otherMass_bc / thisMass_bc + thisVelocity_bc - velocityOfThisInImpactDirection;

			this._currentVelocity = newVelocityOfThisPO;


			_collisionsThisUpdate.Add(version1);
			_collisionsThisUpdate.Add(version2);
			otherPO._collisionsThisUpdate.Add(version1);
			otherPO._collisionsThisUpdate.Add(version2);

		}
	}

	private void ApplyVelocityAndDrag()
	{
		if (_currentVelocity.magnitude > 0)
		{
			transform.position += _currentVelocity * Time.deltaTime * _velocityScaling;
			_currentVelocity = _currentVelocity * (1 - _drag * Time.deltaTime * 10);
			if (_currentVelocity.magnitude < _minSpeed)
			{
				_currentVelocity = new Vector3(0,0,0);
			}
		}

		if (_oldPosition != transform.position)
		{
			_myColliderRealisation.UpdateColliderInfoForTranslation(transform.position - _oldPosition);

			_oldPosition = transform.position;
		}
	}
	
	public void Rotate(Quaternion rotationQuaternion)
	{
		transform.rotation = transform.rotation * rotationQuaternion;
	}


	/// <summary>
	/// rotates the mesh with a rotation matrix. mesh is neither scaled nor translated. Additionally there is a problem with
	/// the mesh instantiation when using this method.
	/// </summary>
	/// <param name="rotationMatrix"></param>
	public void Rotate(float[,] rotationMatrix)
	{
		// Get instantiated mesh
		Mesh mesh = GetComponent<MeshFilter>().mesh;
		// Randomly change vertices
		Vector3[] vertices = mesh.vertices;
		int p = 0;
		while (p < vertices.Length)
		{
			vertices[p] = MyMath.LinearMapping_Mx(rotationMatrix, vertices[p]);
			p++;
		}
		mesh.vertices = vertices;
		mesh.RecalculateNormals();

		//Vector3 newScale = MyMath.LinearMapping_Mx(rotationMatrix, transform.localScale);

		//transform.localScale = newScale;
	}

}
