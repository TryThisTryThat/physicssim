using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(PhysicalObject))]
public class ObjectBuilderEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		PhysicalObject myScript = (PhysicalObject)target;

		//if (GUILayout.Button("Rotate"))
		//{
		//	myScript.Rotate(myScript._currentRotationFromStart);
		//	myScript._myColliderRealisation.UpdateColliderInfoForRotation(myScript._currentRotationFromStart);
		//}

		//if(GUILayout.Button("Rotate with matrix"))
		//{
		//	float w = myScript._currentRotationFromStart.w;
		//	float x = myScript._currentRotationFromStart.x;
		//	float y = myScript._currentRotationFromStart.y;
		//	float z = myScript._currentRotationFromStart.z;

		//	float[,] rotationMatrix = MyMath.GetRotationMatrixFromQuaternion(myScript._currentRotationFromStart);
		//	myScript.Rotate(rotationMatrix);
		//}

		//if(GUILayout.Button("Apply external force"))
		//{
		//	myScript.ApplyIncomingCollision(myScript._incomingVelocity, myScript._incomingMass);
		//}

		if(GUILayout.Button("Apply external rotation"))
		{
			myScript.AddAxisRotation(myScript._normalOfIncomingRotation, myScript._incomingRotationSpeed);
		} 

	}
}

