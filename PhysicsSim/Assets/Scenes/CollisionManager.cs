using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//MONOBEHAVIOUR??
/// <summary>
/// Handles collisiontriggering. 
/// </summary>
public class CollisionManager : MonoBehaviour
{
	[SerializeField]
	public PhysicalObject[] _PO_Array;

	public void GlobalCollisionCheck(PhysicalObject physicalObject)
	{
		foreach (var po in _PO_Array)
		{
			if (po != physicalObject)
			{
				Vector3 p1 = po.transform.position;
				Vector3 p2 = physicalObject.transform.position;

				float sqrDistance = (p1 - p2).x * (p1 - p2).x + (p1 - p2).y * (p1 - p2).y + (p1 - p2).z * (p1 - p2).z;

				MyColliderRealisation col1 = po.GetComponent<MyColliderRealisation>();
				MyColliderRealisation col2 = physicalObject.GetComponent<MyColliderRealisation>();

				if (sqrDistance < (col1._radius + col2._radius) * (col1._radius + col2._radius))
				{
					if (CollisionCheck(col1, col2))
					{
						Debug.Log("There is a collision between " + po.gameObject + " and " + physicalObject.gameObject);

						physicalObject.ApplyIncomingCollision(po);

						//Vector3 locationOfCollision = GetCollisionPoint(col1, col2);
						///Debug.Log("The collision is at least taking place at" + locationOfCollision);
					}
				}
			}
		}
	}
	

	public bool CollisionCheck(MyColliderRealisation collider1, MyColliderRealisation collider2)
	{
		string typ1 = collider1._nameOfVisualisation;
		string typ2 = collider2._nameOfVisualisation;

		if (typ1 == typ2)
		{
			if (typ1 == KnownVisualisations._name_myOwnSphere)
			{
				return CollisionCheckBetweenTwoSpheres(collider1, collider2);
			}
			else if (typ1 == KnownVisualisations._name_myOwnCube)
			{
				return CollisionCheckBetweenTwoCubes(collider1, collider2);
			}
			else
			{
				throw new Exception("Visualisation names are the same but neither cube nor sphere.");
			}
		}
		else
		{
			if (typ1 == KnownVisualisations._name_myOwnSphere && typ2 == KnownVisualisations._name_myOwnCube)
			{
				return CollisionCheckBetweenSphereAndCube(collider1, collider2);
			}
			else if  (typ1 == KnownVisualisations._name_myOwnCube && typ2 == KnownVisualisations._name_myOwnSphere)
			{
				return CollisionCheckBetweenSphereAndCube(collider2, collider1);
			}
			else
			{
				throw new Exception("Unknown visualisation collision.");
			}
		}
	}

	/// <summary>
	/// checks if a sphere and a cube are colliding. this should (not 100%) sure only happen if the nearest point of the sphere is in the cube.
	/// </summary>
	/// <param name="sphere"></param>
	/// <param name="cube"></param>
	/// <returns></returns>
	private bool CollisionCheckBetweenSphereAndCube(MyColliderRealisation sphere, MyColliderRealisation cube)
	{
		Vector3 normalizedDirection = (cube.transform.position - sphere.transform.position) / (cube.transform.position - sphere.transform.position).magnitude;

		Vector3 nearestPointToCubeOnSphere = normalizedDirection * sphere._sphereRadius + sphere.transform.position;

		if (cube.PointIsInCollider(nearestPointToCubeOnSphere))
		{
			return true;
		}

		return ProjectSphereOnCube(cube, sphere);
	}

	private bool ProjectSphereOnCube(MyColliderRealisation cube, MyColliderRealisation sphere)
	{
		Vector3[] vectorsFromFirstEdge = GetDifferenceVectorsToFirst(cube._cubeVertices);

		Vector3[] projectionEdges = MyMath.GetBiggestOrthogonalSubset(vectorsFromFirstEdge);
		
		float[] intervalCube = new float[2];
		float[] intervalSphere = new float[2];

		for (int i = 0; i < projectionEdges.Length; i++)
		{
			Vector3[] spherePointsToProject = new Vector3[2];
			spherePointsToProject[0] = sphere._sphereCenter + (projectionEdges[i]).normalized * sphere._radius;
			spherePointsToProject[1] = sphere._sphereCenter - (projectionEdges[i]).normalized * sphere._radius;

			intervalSphere = GetIntervalThroughProjection(projectionEdges[i], spherePointsToProject);

			Vector3[] edgesOfCube1 = new Vector3[2];

			edgesOfCube1[0] = projectionEdges[0];
			edgesOfCube1[1] = projectionEdges[i];

			intervalCube = GetIntervalThroughProjection(projectionEdges[i], edgesOfCube1);

			if (!IntervalIntersection(intervalCube, intervalSphere))
			{
				return false;
			}
		}

		return true;
	}

	private Vector3[] GetDifferenceVectorsToFirst(Vector3[] v3Array)
	{
		if (v3Array.Length <2)
		{
			throw new Exception("length of input array has to be at least 2");
		}

		Vector3[] diffArray = new Vector3[v3Array.Length - 1];

		for (int i = 0; i < diffArray.Length; i++)
		{
			diffArray[i] = v3Array[i + 1] - v3Array[0];
		}

		return diffArray;
	}

	private bool CollisionCheckBetweenTwoCubes(MyColliderRealisation cube1, MyColliderRealisation cube2)
	{
		if (ProjectSecondCubeOnFirstCube(cube1, cube2) && ProjectSecondCubeOnFirstCube(cube2, cube1))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	private bool CollisionCheckBetweenTwoCubes(MyColliderRealisation cube1, MyColliderRealisation cube2, out Vector3 collisionPoint)
	{
		if (ProjectSecondCubeOnFirstCube(cube1, cube2) && ProjectSecondCubeOnFirstCube(cube2, cube1))
		{
			collisionPoint = GetCollisionPoint(cube1, cube2);
			return true;
		}
		else
		{
			collisionPoint = new Vector3();
			return false;
		}
	}

	private Vector3 GetCollisionPoint(MyColliderRealisation cube1, MyColliderRealisation cube2)
	{
		Vector3[] vectorsFromFirstVertex = GetDifferenceVectorsToFirst(cube1._cubeVertices);

		//get the three defining edges from first vertex in vertex array
		Vector3[] projectionEdges = MyMath.GetBiggestOrthogonalSubset(vectorsFromFirstVertex);

		float[] intervalCube1 = new float[2];
		float[] intervalCube2 = new float[2];

		//infoForCollisionDetection
		Vector3 newCorner = cube1._cubeVertices[0]; //will be adjusted by interval intersections
		float[] distanceArray = new float[3]; //will be adjusted by interval intersections
		Vector3[] directions = new Vector3[3]; //will be adjusted by interval intersections

		for (int i = 0; i < projectionEdges.Length; i++)
		{
			intervalCube2 = GetIntervalThroughProjection(projectionEdges[i], cube2._cubeVertices);

			intervalCube1 = GetIntervalThroughProjection(projectionEdges[i], cube1._cubeVertices);

			float[] intersection;

			IntervalIntersection(intervalCube1, intervalCube2, out intersection);


			intersection[0] -= intervalCube1[0];
			intersection[1] -= intervalCube1[0];


			newCorner += projectionEdges[i].normalized * intersection[0];
			distanceArray[i] = intersection[1] - intersection[0];
			directions[i] = projectionEdges[i].normalized;
		}

		return FindOnePointContainedInCube(newCorner, directions, distanceArray, cube2);
	}

	private Vector3 FindOnePointContainedInCube(Vector3 newCorner, Vector3[] directions, float[] distanceArray, MyColliderRealisation cube)
	{
		float startGranularity = 10;

		float granularity = startGranularity;

		int maxIterations = 2;

		for (int o= 0; o < maxIterations; o++)
		{
			for (int j = 0; j < granularity; j++)
			{
				for (int k = 0; k < granularity; k++)
				{
					for (int l = 0; l < granularity; l++)
					{
						Vector3 point;

						if (o > 1)
						{
							for (int p = 0; p < 3; p++)
							{
								float r1 = UnityEngine.Random.Range(0, 2 * Mathf.PI);
								float r2 = UnityEngine.Random.Range(0, 2 * Mathf.PI);

								Vector3 smallRandomVector = new Vector3(Mathf.Cos(r1), Mathf.Sin(r1) * Mathf.Cos(r2), Mathf.Cos(r2));

								//TODO: remove
								float justTest = smallRandomVector.magnitude;

								float averageOfDistanceArray = (distanceArray[0] + distanceArray[1] + distanceArray[2]) / 3;

								point = newCorner + directions[0] * j / granularity * distanceArray[0] +
								directions[1] * k / granularity * distanceArray[1] +
								directions[2] * l / granularity * distanceArray[2] + smallRandomVector / granularity * distanceArray.Min();

								if (cube.PointIsInCollider(point))
								{
									return point;
								}
							}
						}
						else
						{
							point = newCorner + directions[0] * j / granularity * distanceArray[0] +
							directions[1] * k / granularity * distanceArray[1] +
							directions[2] * l / granularity * distanceArray[2];

							if (cube.PointIsInCollider(point))
							{
								return point;
							}
						}
					}
				}
			}
		}

		return newCorner; //TODO: not good!!
		//throw new Exception("Somethin went wrong");
	}

	/// <summary>
	/// returns true if the projection of the second cube on the first cube using all three axis of the first cube always have an interseciton
	/// 
	/// </summary>
	/// <param name="cube1"></param>
	/// <param name="cube2"></param>
	/// <returns></returns>
	private bool ProjectSecondCubeOnFirstCube(MyColliderRealisation cube1, MyColliderRealisation cube2)
	{
		Vector3[] vectorsFromFirstEdge = GetDifferenceVectorsToFirst(cube1._cubeVertices);

		Vector3[] projectionEdges = MyMath.GetBiggestOrthogonalSubset(vectorsFromFirstEdge);

		float[] intervalCube1 = new float[2];
		float[] intervalCube2 = new float[2];
		
		for (int i = 0; i < projectionEdges.Length; i++)
		{
			intervalCube2 = GetIntervalThroughProjection(projectionEdges[i], cube2._cubeVertices);
			
			intervalCube1 = GetIntervalThroughProjection(projectionEdges[i], cube1._cubeVertices);

			if (!IntervalIntersection(intervalCube1, intervalCube2))
			{
				return false;
			}
		}

		return true;
	}

	private bool IntervalIntersection(float[] interval1, float[] interval2)
	{
		if (interval1[0] > interval1[1])
		{
			throw new Exception("something wrong with intervals in projection");
		}

		if (interval2[0] > interval2[1])
		{
			throw new Exception("something wrong with intervals in projection");
		}

		float min1 = interval1[0];
		float max1 = interval1[1];

		float min2 = interval2[0];
		float max2 = interval2[1];

		if (max1 < min2)
		{
			return false;
		}
		else if (max2 < min1)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	private bool IntervalIntersection(float[] interval1, float[] interval2, out float[] intersection)
	{
		if (interval1[0] > interval1[1])
		{
			throw new Exception("something wrong with intervals in projection");
		}

		if (interval2[0] > interval2[1])
		{
			throw new Exception("something wrong with intervals in projection");
		}

		float min1 = interval1[0];
		float max1 = interval1[1];

		float min2 = interval2[0];
		float max2 = interval2[1];

		if (max1 < min2)
		{
			intersection = new float[2] { 0, 0 };
			return false;
		}
		else if (max2 < min1)
		{
			intersection = new float[2] { 0, 0 };
			return false;
		}
		else
		{
			if (max2-min1 < max1 - min2)
			{
				intersection = new float[2] { min1, max2 };
			}
			else
			{
				intersection = new float[2] { min2, max1 };
			}
			
			return true;
		}
	}

	/// <summary>
	/// project all points on the axis (with orientation) and returns the smallest interval the projected points lie in.
	/// </summary>
	/// <param name="axisDefiner0Point"></param>
	/// <param name="axisDefiner1Point"></param>
	/// <param name="pointsToProject"></param>
	/// <returns></returns>
	private float[] GetIntervalThroughProjection(Vector3 axisDefiner0Point, Vector3 axisDefiner1Point, Vector3[] pointsToProject)
	{
		float[] projectedPositionsIn1D = new float[pointsToProject.Length];

		float[] minMax = new float[2];

		Vector3 lineDefiner = (axisDefiner1Point - axisDefiner0Point).normalized;

		for (int i = 0; i < pointsToProject.Length; i++)
		{
			projectedPositionsIn1D[i] = Vector3.Dot(lineDefiner, pointsToProject[i]);
		}

		minMax[0] = projectedPositionsIn1D.Min();
		minMax[1] = projectedPositionsIn1D.Max();

		return minMax;
	}

	/// <summary>
	/// project all points on the axis (with orientation) and returns the smallest interval the projected points lie in.
	/// </summary>
	/// <param name="projectionAxisDefiner"></param>
	/// <param name="pointsToProject"></param>
	/// <returns></returns>
	private float[] GetIntervalThroughProjection(Vector3 projectionAxisDefiner, Vector3[] pointsToProject)
	{
		float[] projectedPositionsIn1D = new float[pointsToProject.Length];

		float[] minMax = new float[2];

		Vector3 lineDefiner = (projectionAxisDefiner).normalized;

		for (int i = 0; i < pointsToProject.Length; i++)
		{
			projectedPositionsIn1D[i] = Vector3.Dot(lineDefiner, pointsToProject[i]);
		}

		minMax[0] = projectedPositionsIn1D.Min();
		minMax[1] = projectedPositionsIn1D.Max();

		return minMax;
	}

	private bool CollisionCheckBetweenTwoSpheres(MyColliderRealisation col1, MyColliderRealisation col2)
	{
		if ((col1._sphereCenter - col2._sphereCenter).magnitude < col1._sphereRadius + col2._sphereRadius)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
