using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class MyMath
{
	public static Vector3[] GetBiggestOrthogonalSubset(Vector3[] vectors)
	{
		if (vectors.Length == 0)
		{
			throw new Exception("Input array has to be non-empty.");
		}

		if (vectors.Length == 1)
		{
			return vectors;
		}


		List<Vector3[]>[] foundOrthogonalSubsets = new List<Vector3[]>[vectors.Length];

		foundOrthogonalSubsets = AddTwoSubsets(foundOrthogonalSubsets, vectors);

		if (foundOrthogonalSubsets[0].Count == 0)
		{
			throw new Exception("No orthogonal subset found");
		}

		foundOrthogonalSubsets = ListAllOrthogonalSubsets(foundOrthogonalSubsets, vectors);

		for (int o = vectors.Length - 3; o > 0; o--) //TODO: what should be the correct max lenght here to start from?
		{
			if (foundOrthogonalSubsets[o] != null && foundOrthogonalSubsets[o].Count > 0)
			{
				return foundOrthogonalSubsets[o][0];
			}
		}

		throw new Exception("Something didnt work!..");
	}

	private static List<Vector3[]>[] AddTwoSubsets(List<Vector3[]>[] orthSubsetListArray, Vector3[] vectors)
	{
		//start with subsets containing at least two subsets
		for (int i = 0; i < vectors.Length; i++)
		{
			for (int j = 0; j < vectors.Length; j++)
			{
				if (i != j)
				{
					if (Vector3.Dot(vectors[i], vectors[j]) == 0)
					{
						if (orthSubsetListArray[0] == null)
						{
							orthSubsetListArray[0] = new List<Vector3[]>();
						}

						Vector3[] twoSubset = { vectors[i], vectors[j] };
						orthSubsetListArray[0].Add(twoSubset);
					}
				}
			}
		}

		return orthSubsetListArray;
	}

	private static List<Vector3[]>[] ListAllOrthogonalSubsets(List<Vector3[]>[] orthSubsetListArray, Vector3[] vectors)
	{
		if (vectors.Length > 2)
		{
			//max length of return subset
			for (int k = 0; k < vectors.Length - 1; k++) //TODO: Check length probably worng
			{
				//go through every given vector
				for (int i = 0; i < vectors.Length; i++)
				{
					if (orthSubsetListArray[k] == null)
					{
						break;
					}

					//go through every list item
					for (int j = 0; j < orthSubsetListArray[k].Count; j++)
					{
						Vector3[] combined = orthSubsetListArray[k][j].Concat(new Vector3[1] { vectors[i] }).ToArray();
						if (IsOrthogonalSystem(combined))
						{
							if (orthSubsetListArray[k + 1] == null)
							{
								orthSubsetListArray[k + 1] = new List<Vector3[]>();
							}

							orthSubsetListArray[k + 1].Add(combined);
						}
					}
				}
			}
		}

		return orthSubsetListArray;
	}

	public static bool IsOrthogonalSystem(Vector3[] vectors)
	{
		for (int i = 0; i < vectors.Length; i++)
		{
			for (int j = 0; j < vectors.Length; j++)
			{
				if (i != j)
				{
					if (Vector3.Dot(vectors[i], vectors[j]) != 0)
					{
						return false;
					}
				}
			}
		}

		return true;
	}

	public static float[,] GetRotationMatrixFromQuaternion(Quaternion quaternion)
	{
		float w = quaternion.w;
		float x = quaternion.x;
		float y = quaternion.y;
		float z = quaternion.z;

		float n = w * w + x * x + y * y + z * z;
		float s = 0;
		if (n == 0)
		{
			s = 0;
		}
		else
		{
			s = 2 / n;
		}

		float wx = s * w * x, wy = s * w * y, wz = s * w * z,
		xx = s * x * x, xy = s * x * y, xz = s * x * z,
		yy = s * y * y, yz = s * y * z, zz = s * z * z;

		return new float[3, 3] { { 1 - (yy + zz), xy - wz, xz + wy},
								 { xy + wz, 1 - (xx + zz), yz - wx},
								 { xz - wy, yz + wx, 1 - (xx + yy)} };
	}

	public static float Magnitude(Vector3 input)
	{
		return Mathf.Sqrt(input.x * input.x + input.y * input.y + input.z * input.z);
	}

	public static float sqrMagnitude(Vector3 input)
	{
		return input.x * input.x + input.y * input.y + input.z * input.z;
	}

	public static float EnclosingAngle(Vector3 v1, Vector3 v2)
	{
		return Mathf.Acos(DotProduct(v1, v2) / (Magnitude(v1) * Magnitude(v2)));
	}

	public static float DotProduct(Vector3 v1, Vector3 v2)
	{
		return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
	}

	public static Vector3 Normalize(Vector3 v)
	{
		return ScMult(1 / Magnitude(v), v);
	}

	public static Vector3 ScMult(float f, Vector3 v)
	{
		Vector3 w = new Vector3(f * v.x, f * v.y, f * v.z);
		return w;
	}

	/// <summary>
	/// Matrix multiplication. Length of first dimension of the first matrix must be the same as zero dimension of second matrix.
	/// </summary>
	/// <param name="leftMatrix"></param>
	/// <param name="rightMatrix"></param>
	/// <returns></returns>
	public static float[,] MatrixMult(float[,] leftMatrix, float[,] rightMatrix)
	{
		int lm_rows = leftMatrix.GetLength(0);
		int lm_columns = leftMatrix.GetLength(1);

		int rm_rows = rightMatrix.GetLength(0);
		int rm_columns = rightMatrix.GetLength(1);

		if (lm_columns != rm_rows)
		{
			return null;
		}

		float[,] resultMatrix = new float[lm_rows, rm_columns];

		for (int i = 0; i < lm_rows; i++)
		{
			for (int j = 0; j < rm_columns; j++)
			{
				float sumOfProducts = 0;
				for (int k = 0; k < lm_columns; k++)
				{
					sumOfProducts += leftMatrix[i, k] * rightMatrix[k, j];
				}
				resultMatrix[i, j] = sumOfProducts;
			}

		}

		return resultMatrix;
	}

	/// <summary>
	/// matrix (r1,r2,r3) * column vector v. r1,..,r3 are row vectors
	/// </summary>
	/// <param name="v"></param>
	/// <param name="r1"></param>
	/// <param name="r2"></param>
	/// <param name="r3"></param>
	/// <returns></returns>
	public static Vector3 LinearMapping(Vector3 r1, Vector3 r2, Vector3 r3, Vector3 v)
	{
		Vector3 w = new Vector3();
		w.x = DotProduct(v, r1);
		w.y = DotProduct(v, r2);
		w.z = DotProduct(v, r3);

		return w;
	}

	/// <summary>
	/// Linear mappings in three dimensional space. matrix*vector
	/// </summary>
	/// <param name="matrix"></param>
	/// <param name="v"></param>
	/// <returns></returns>
	public static Vector3 LinearMapping(float[,] matrix, Vector3 v)
	{
		int m_rows = matrix.GetLength(0);
		int m_columns = matrix.GetLength(1);

		if (m_rows != 3 || m_columns != 3)
		{
			throw new Exception("Linear Mapping needs 3x3 matrix.");
		}

		Vector3 r1 = new Vector3(matrix[0, 0], matrix[0, 1], matrix[0, 2]);
		Vector3 r2 = new Vector3(matrix[1, 0], matrix[1, 1], matrix[1, 2]);
		Vector3 r3 = new Vector3(matrix[2, 0], matrix[2, 1], matrix[2, 2]);

		return LinearMapping(r1, r2, r3, v);
	}

	/// <summary>
	/// Linear mappings in three dimensional space. row vector * 3x3matrix
	/// </summary>
	/// <param name="matrix"></param>
	/// <param name="v"></param>
	/// <returns></returns>
	public static Vector3 LinearMapping_xM(Vector3 v, float[,] matrix)
	{
		int m_rows = matrix.GetLength(0);
		int m_columns = matrix.GetLength(1);

		if (m_rows != 3 || m_columns != 3)
		{
			throw new Exception("Linear Mapping needs 3x3 matrix.");
		}

		Vector3 w = new Vector3();

		w.x = v.x * matrix[0, 0] + v.y * matrix[1, 0] + v.z * matrix[2, 0];
		w.y = v.x * matrix[0, 1] + v.y * matrix[1, 1] + v.z * matrix[2, 1];
		w.z = v.x * matrix[0, 2] + v.y * matrix[1, 2] + v.z * matrix[2, 2];

		return w;
	}

	/// <summary>
	/// Linear mappings in three dimensional space. 3x3matrix * column vector
	/// </summary>
	/// <param name="matrix"></param>
	/// <param name="v"></param>
	/// <returns></returns>
	public static Vector3 LinearMapping_Mx(float[,] matrix, Vector3 v)
	{
		int m_rows = matrix.GetLength(0);
		int m_columns = matrix.GetLength(1);

		if (m_rows != 3 || m_columns != 3)
		{
			throw new Exception("Linear Mapping needs 3x3 matrix.");
		}

		Vector3 w = new Vector3();

		w.x = v.x * matrix[0, 0] + v.y * matrix[0, 1] + v.z * matrix[0, 2];
		w.y = v.x * matrix[1, 0] + v.y * matrix[1, 1] + v.z * matrix[1, 2];
		w.z = v.x * matrix[2, 0] + v.y * matrix[2, 1] + v.z * matrix[2, 2];

		return w;
	}

	/// <summary>
	/// inverse of 3x3 matrices
	/// </summary>
	/// <param name="matrix"></param>
	/// <returns></returns>
	public static float[,] Invert(float[,] matrix)
	{
		int r = matrix.GetLength(0);
		int c = matrix.GetLength(0);

		if (r != c)
		{
			throw new Exception("Someone tried to apply matrix inversion on a non quadratic matrix.");
		}

		float[,] inverse = new float[r, r];

		float factor = 1 / Determinant(matrix);

		for (int i = 0; i < r; i++)
		{
			for (int j = 0; j < r; j++)
			{
				inverse[i, j] = factor * (float)Math.Pow(-1, i + j + 2) * Determinant(DeletionSubmatrix(matrix, j, i));
			}
		}

		return inverse;
	}

	/// <summary>
	/// inverse of 3x3 matrices
	/// </summary>
	/// <param name="matrix"></param>
	/// <returns></returns>
	public static float Determinant(float[,] matrix)
	{
		int rows = matrix.GetLength(0);
		int columns = matrix.GetLength(1);

		if (rows != columns)
		{
			throw new Exception("Someone tried to calculate the determinant of a non quadratic matrix.");
		}

		if (rows == 1)
		{
			return matrix[0, 0];
		}

		float det = 0;

		for (int i = 0; i < columns; i++)
		{
			det += matrix[0, i] * (float)Math.Pow(-1, i) * Determinant(DeletionSubmatrix(matrix, 0, i));
		}

		return det;
	}

	public static float[,] DeletionSubmatrix(float[,] matrix, int row, int column)
	{
		int rows = matrix.GetLength(0);
		int columns = matrix.GetLength(1);

		if (rows == 1 || columns == 1)
		{
			float[,] one = new float[1, 1];
			one[0, 0] = 1;
			return one;
		}

		float[,] deletionSubmatrix = new float[rows - 1, columns - 1];

		for (int i = 0; i < rows - 1; i++)
		{
			for (int j = 0; j < columns - 1; j++)
			{
				int a = i;
				int b = j;
				if (i >= row)
				{
					a++;
				}
				if (j >= column)
				{
					b++;
				}

				deletionSubmatrix[i, j] = matrix[a, b];
			}
		}

		return deletionSubmatrix;
	}


	public static Vector3 CrossProduct(Vector3 v1, Vector3 v2)
	{
		Vector3 w = new Vector3();

		w.x = v1.y * v2.z - v1.z * v2.y;
		w.y = v1.z * v2.x - v1.x * v2.z;
		w.z = v1.x * v2.y - v2.x * v1.y;

		return w;
	}

	public static Vector3 GetNormalOfPlane(Vector3 lhs, Vector3 rhs)
	{
		Vector3 normal = Vector3.Cross(lhs, rhs);

		if (normal == new Vector3(0,0,0))
		{
			throw new Exception("input vectors have to be linear independent.");
		}
		else
		{
			normal = normal.normalized;
		}

		return normal;
	}

	public static float[,] GetRotationMatrixFromNormalAndAngle(Vector3 u, float t)
	{
		if (u.magnitude != 1)
		{
			throw new Exception("vector has to be a unit vector");
		}
		
		float[,] matrix = new float[3, 3];

		float cos_t = Mathf.Cos(t);
		float sin_t = Mathf.Sin(t);

		matrix[0, 0] = cos_t + u.x * u.x*(1 - cos_t);
		matrix[0, 1] = u.x * u.y*(1 - cos_t) - u.z * sin_t;
		matrix[0, 2] = u.x * u.z* (1- cos_t) + u.y * sin_t;
		matrix[1, 0] = u.y * u.x * (1 - cos_t) + u.z * sin_t;
		matrix[1, 1] = cos_t + u.y * u.y * (1 - cos_t);
		matrix[1, 2] = u.y * u.z * (1 - cos_t) - u.x * sin_t;
		matrix[2, 0] = u.z * u.x * (1 - cos_t) - u.y * sin_t;
		matrix[2, 1] = u.z * u.y * (1 - cos_t) + u.x * sin_t;
		matrix[2, 2] = cos_t + u.z * u.z * (1 - cos_t);

		return matrix;
	}

	public static bool ExactlyOneDifference(Vector3 v, Vector3 w)
	{
		if (v.x != w.x && v.y != w.y)
		{
			return false;
		}
		else if (v.x != w.x && v.z != w.z)
		{
			return false;
		}
		else if (v.y != w.y && v.z != w.z)
		{
			return false;
		}
		else if (v.x == w.x && v.y == w.y && v.z == w.z)
		{
			return false;
		}

		return true;
	}



}
