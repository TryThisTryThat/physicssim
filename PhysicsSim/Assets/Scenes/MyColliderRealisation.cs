using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyColliderRealisation : MonoBehaviour
{
	public float _radius;

	//has to be filled in editor.
	[SerializeField]
	public string _nameOfVisualisation;

	//Only used if sphere.
	[NonSerialized]
	public Vector3 _sphereCenter;
	[NonSerialized]
	public float _sphereRadius;

	//only used if cube.
	public Vector3[] _cubeVertices;
	[NonSerialized]
	public Vector3 _cubeCenter;
	private Vector3 _cubeHalfSideLength;

	void Awake ()
	{
		InitiateGlobalInformation();
	}

	/// <summary>
	/// Initiates all global fields including the inherited.
	/// </summary>
	private void InitiateGlobalInformation()
	{
		if (_nameOfVisualisation == KnownVisualisations._name_myOwnCube)
		{
			InitiateCubeFields();
		}
		else if (_nameOfVisualisation == KnownVisualisations._name_myOwnSphere)
		{
			_sphereCenter = transform.position;

			if (transform.lossyScale.x != transform.lossyScale.y || transform.lossyScale.x != transform.lossyScale.z)
			{
				throw new Exception("scale of sphere not homogeneous.");
			}

			_sphereRadius = transform.lossyScale.x * 0.5f;
			_radius = _sphereRadius;
		}
	}

	
	private void InitiateCubeFields()
	{
		_cubeCenter = transform.position;

		_cubeVertices = new Vector3[8];
		float xScale = transform.lossyScale.x;
		float yScale = transform.lossyScale.y;
		float zScale = transform.lossyScale.z;

		_cubeHalfSideLength.x = xScale * 0.5f;
		_cubeHalfSideLength.y = yScale * 0.5f;
		_cubeHalfSideLength.z = zScale * 0.5f;

		float[] halfScaleArray = new float[3];
		halfScaleArray[0] = xScale * 0.5f;
		halfScaleArray[1] = yScale * 0.5f;
		halfScaleArray[2] = zScale * 0.5f;
		Array.Sort(halfScaleArray);

		_radius = Mathf.Sqrt(halfScaleArray[1] * halfScaleArray[1] + halfScaleArray[2] * halfScaleArray[2]);

		int l = 0;

		for (int i = 0; i < 2; i++)
		{
			for (int j = 0; j < 2; j++)
			{
				for (int k = 0; k < 2; k++)
				{
					_cubeVertices[l] = transform.position - transform.lossyScale * 0.5f + new Vector3(i * xScale, j * yScale, k * zScale);
					l++;
				}
			}
		}
	}

	/// <summary>
	/// updates the information about the collider if a simple translation has occured.
	/// </summary>
	/// <param name="translation"></param>
	internal void UpdateColliderInfoForTranslation(Vector3 translation)
	{
		if (_nameOfVisualisation == KnownVisualisations._name_myOwnCube)
		{
			for (int i = 0; i < _cubeVertices.Length; i++)
			{
				_cubeVertices[i] += translation;
			}

			_cubeCenter += translation;
		}
		else if(_nameOfVisualisation == KnownVisualisations._name_myOwnSphere)
		{
			_sphereCenter += translation;
		}
	}

	internal void UpdateColliderInfoForRotation(Quaternion rotation)
	{
		if (_nameOfVisualisation == KnownVisualisations._name_myOwnCube)
		{
			float[,] rotationMatrix3x3 = MyMath.GetRotationMatrixFromQuaternion(rotation);

			Vector3[] translatedEdges = new Vector3[8];

			for (int i = 0; i < translatedEdges.Length; i++)
			{
				translatedEdges[i] = _cubeVertices[i] - _cubeCenter;
				translatedEdges[i] = MyMath.LinearMapping_Mx(rotationMatrix3x3, translatedEdges[i]);
				_cubeVertices[i] = translatedEdges[i] + _cubeCenter;
 			}
		}
	}


	/// <summary>
	/// Checks if given point is in or touches the collider.
	/// </summary>
	/// <param name="point"></param>
	/// <returns></returns>
	public bool PointIsInCollider(Vector3 point)
	{
		switch (_nameOfVisualisation)
		{
			case "Cube":
				return PointIsInCubeCollider(point);
				
			case "Sphere":
				return PointIsInSphereCollider(point);

			default:
				throw new Exception("Mesh is not known. No MyCollider defined for this Mesh.");
		}
	}

	private bool PointIsInSphereCollider(Vector3 point)
	{
		if ((point - _sphereCenter).magnitude < _sphereRadius)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	private bool PointIsInCubeCollider(Vector3 point)
	{
		Vector3 relPoint = point - _cubeCenter;

		float w = transform.rotation.w;
		float x = transform.rotation.x;
		float y = transform.rotation.y;
		float z = transform.rotation.z;

		float sqrNorm = w * w + x * x + y * y + z * z;

		Quaternion inverseRotation = new Quaternion();

		if (!(sqrNorm == 0))
		{
			inverseRotation.w = w / sqrNorm;
			inverseRotation.x = -x / sqrNorm;
			inverseRotation.y = -y / sqrNorm;
			inverseRotation.z = -z / sqrNorm;
		}

		relPoint = MyMath.LinearMapping_Mx(MyMath.GetRotationMatrixFromQuaternion(inverseRotation), relPoint);

		if (Mathf.Abs(relPoint.x) > _cubeHalfSideLength.x)
		{
			return false;
		}
		else if (Mathf.Abs(relPoint.y) > _cubeHalfSideLength.y)
		{
			return false;
		}
		else if (Mathf.Abs(relPoint.z) > _cubeHalfSideLength.z)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	private void OnTriggerEnter(Collider other)
	{

	}
}
