using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class to store names of my known visualisations of physical objects.
/// </summary>
public static class KnownVisualisations
{
	public static string _name_myOwnCube = "Cube";
	public static string _name_myOwnSphere = "Sphere";
}
